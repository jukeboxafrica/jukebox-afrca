<?php

use Illuminate\Database\Seeder;

class ArtistSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        //
        $artist=[
            [
                'ref_id'=> 1 ,
                'firstname'=>'Andrew',
                'lastname'=>'Chamamme',
                'email'=>'andrew@mail.com',
                'password'=> bcrypt('woofer'),
                'title' => 'The King',
                'biography'=>'He is the king of the game',
                'image'=>'king.jpg',
            ],
            [
                'ref_id'=> 2 ,
                'firstname'=>'John',
                'lastname'=>'Doe',
                'email'=>'john@mail.com',
                'password'=> bcrypt('woofer'),
                'title' => 'The King',
                'biography'=>'He is the king of the game',
                'image'=>'king.jpg',
            ]
        ];

        foreach ($artist as $key => $value) {
            \App\Artist::create($value);
        }
    }
}
