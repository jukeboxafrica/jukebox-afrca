<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

//Route::group(['prefix' => 'administration','middleware'=>['admin']], function () {
//    Route::get('/', function () {
//        dd('This is the Administration module index page. Build something great!');
//    });
//
//    Route::get('/home', function () {
////    $users[] = Auth::user();
////    $users[] = Auth::guard()->user();
////    $users[] = Auth::guard('admin')->user();
//
//        //dd($users);
//
//        return view('admin.home');
//    })->name('home');
//});
Route::group(['prefix' => 'admin','middleware'=>['admin'],'as'=>'admin.'], function () {
    Route::get('/', function () {
        dd('This is the Administration module index page. Build something great!');
    });

    Route::get('/home', function () {
        return redirect( route('dashboard'));
//    $users[] = Auth::user();
//    $users[] = Auth::guard()->user();
//    $users[] = Auth::guard('admin')->user();
//        dd($users);
        return view('administration::home');
    })->name('home');


    Route::resource('song','SongsController');
    Route::get('dashboard', 'DashboardController@index')->name('dashboard');
});


Route::group(['namespace' => 'App\Modules\Administration\Http\Controllers','middleware' => ['web','auth'],'prefix' => '/backend'],
    function() {
        // auth roles and permissions route
//        Route::get('/dashboard', function(){
//            echo "Hello World";
//        });


    });


Route::group(['prefix' => 'admin'], function () {

    Route::get('/login', 'AdminAuth\LoginController@showLoginForm')->name('admin.login');;
    Route::post('/login', 'AdminAuth\LoginController@login')->name('admin.login');;
    Route::post('/logout', 'AdminAuth\LoginController@logout')->name('admin.logout');;

    Route::get('/register', 'AdminAuth\RegisterController@showRegistrationForm')->name('admin.register');;
    Route::post('/register', 'AdminAuth\RegisterController@register')->name('admin.register');;

    Route::post('/password/email', 'AdminAuth\ForgotPasswordController@sendResetLinkEmail')->name('admin.password.reset.link');;
    Route::post('/password/reset', 'AdminAuth\ResetPasswordController@reset')->name('admin.password.reset');
    Route::get('/password/reset', 'AdminAuth\ForgotPasswordController@showLinkRequestForm')->name('admin.password.reset');
    Route::get('/password/reset/{token}', 'AdminAuth\ResetPasswordController@showResetForm');
});