<?php

use Illuminate\Database\Seeder;

class AlbumSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        //
        $albums=[
            [
                'ref_id'=> 1 ,
                'title'=>'Sorry',
                'description'=>'The top album',
                'artist_id'=>'1',
                'image'=> 'album1',
                'directory'=>'albums',
            ],
        ];

        foreach ($albums as $key => $value) {
            \App\Album::create($value);
        }
    }
}
