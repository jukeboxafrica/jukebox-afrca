@extends('administration::layout.main')
@section('content')
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Add Song</h3>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_content">
                            <form name="item" method="post" action="{{route('admin.song.store')}}" enctype="multipart/form-data" >
                                <input type="hidden" name="_method" value="POST">
                                {{csrf_field()}}
                                <div class="card card-block">
                                    <div class="form-group row">
                                        <label class="col-sm-2 form-control-label text-xs-right">
                                            Title :
                                        </label>
                                        <div class="col-sm-10">
                                            <input type="text" name="title" class="form-control boxed" placeholder=" Song title">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-2 form-control-label text-xs-right">
                                            Album :
                                        </label>
                                        <div class="col-sm-10">
                                            <select name="artist_id" class="c-select form-control boxed">
                                                <option selected>Select Album</option>
                                                @foreach($albums as $album)
                                                    <option value="{{$album->id}}">{{$album->title}}</option>
                                                @endforeach
                                            </select>
                                            {{--<input type="text"  name="album_id" class="form-control boxed" placeholder="Album it belongs to">--}}
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-2 form-control-label text-xs-right">
                                            Artist:
                                        </label>
                                        <div class="col-sm-10">
                                            <select name="artist_id" class="c-select form-control boxed">
                                                <option selected>Select Artist</option>
                                                @foreach($artists as $artist)
                                                    <option value="{{$artist->id}}">{{$artist->firstname}} ({{$artist->title}}) </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-2 form-control-label text-xs-right">
                                            Genre:
                                        </label>
                                        <div class="col-sm-10">
                                            <select name="genre_id[]" class="c-select form-control boxed" multiple>
                                                <option selected>Select Genre</option>
                                                @foreach($genres as $genre)
                                                    <option value="{{$genre->id}}">{{$genre->genre}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-sm-2 form-control-label text-xs-right">
                                            Music file :
                                        </label>
                                        <div class="col-sm-10">
                                            <div class="images-container">
                                                <div class="image-container">
                                                    <input class="" type="file" name="music_file">
                                                    {{--
                                                    @TODO Implement the themes image UI
                                                    <a href="#" class="add-image" data-toggle="modal" data-target="#modal-media">--}}
                                                    {{--<div class="image-container new">--}}
                                                    {{--<div class="image"> <i class="fa fa-plus"></i> </div>--}}
                                                    {{--</div>--}}
                                                    {{--</a>--}}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-2 form-control-label text-xs-right">
                                            Cover Photo:
                                        </label>
                                        <div class="col-sm-10">
                                            <div class="images-container">
                                                <div class="image-container">
                                                    <input class="" type="file" name="image">
                                                    {{--
                                                    @TODO Implement the themes image UI
                                                    <a href="#" class="add-image" data-toggle="modal" data-target="#modal-media">--}}
                                                    {{--<div class="image-container new">--}}
                                                    {{--<div class="image"> <i class="fa fa-plus"></i> </div>--}}
                                                    {{--</div>--}}
                                                    {{--</a>--}}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-10 col-sm-offset-2">
                                            <button type="submit" class="btn btn-primary">
                                                Submit
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection