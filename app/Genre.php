<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Genre extends Model
{
    //
    use SoftDeletes;

    protected $fillable =['genre'];
    protected $dates =['deleted_at' , 'created_at' , 'updated_at'];
    protected  $softDeletes = true;

    /**
     * Gets all the songs of a genre
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function songs(){
        return $this->hasMany('App\Song');
    }
}
