@extends('administration::layout.main')

@section('content')
    <div class="right_col" role="main">

        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Song - {{ $song->title }}</h3>
                </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_content">
                            <div class="col-md-5 col-sm-5 col-xs-12" style="border:0 solid #e5e5e5;">

                                <h3 class="prod_title">{{ $song->title }}</h3>

                                <p>
                                    @if($song->genres->isEmpty())
                                        <i>No genre specified</i>
                                    @else
                                        @foreach($song->genres as $genre)
                                            {{ $genre->genre }}
                                        @endforeach
                                    @endif
                                </p>
                                <br>

                                <div class="">
                                    <h2>Creation Dated</h2>
                                    <ul class="">
                                        <li>
                                            <p>{{ $song->created_at->toFormattedDateString() }}</p>
                                            <div class="color bg-red"></div>
                                        </li>
                                    </ul>
                                </div>
                                <br>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
