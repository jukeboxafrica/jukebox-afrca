<?php

use Illuminate\Database\Seeder;

class GenreSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        //
        $genre=[
            [
                'ref_id'=> 1 ,
                'genre'=>'Hiplife'
            ],
            [
                'ref_id'=> 2 ,
                'genre'=>'Gospel'
            ],
            [
                'ref_id'=> 3 ,
                'genre'=>'Country'
            ]
        ];

        foreach ($genre as $key => $value) {
            \App\Genre::create($value);
        }
    }
}
