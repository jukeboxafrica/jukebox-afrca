<?php
/**
 * Created by PhpStorm.
 * User: Andrew Chamamme
 * Date: 9/1/2016
 * Time: 11:38 AM
 */

Route::group(['namespace' => 'App\Modules\Backend\Controllers','middleware' => ['web','auth'],'prefix' => '/backend'],
    function() {
        // auth roles and permissions route
//        Route::get('/dashboard', function(){
//            echo "Hello World";
//        });

        Route::resource('song','SongsController');
        Route::get('dashboard', 'DashboardController@index');
    });

