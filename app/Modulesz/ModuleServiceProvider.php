<?php
/**
 * Created by PhpStorm.
 * User: edel
 * Date: 8/16/2016
 * Time: 9:44 PM
 */

namespace App\Modules;

use \Illuminate\Support\ServiceProvider;
use \Symfony\Component\Finder\Finder;

class ModuleServiceProvider extends ServiceProvider
{

    /**
     * Load required modules
     */
    public function boot() {

        // boot from config
        $this->boot_reg();

    }

    /**
     * bootstrap module from module config/module
     */
    protected function boot_reg() {
        //load modules from config
        $modules = config('module.modules');
        //dd($modules);
        while(list(, $module) = each($modules)) {

            // load routes file in module
            if(file_exists(__DIR__ . "/{$module}/routes.php")) {
                include_once __DIR__ . "/{$module}/routes.php";
            }

            // load files in Views folder
            if(is_dir(__DIR__ . "/{$module}/Views")) {
                $this->loadViewsFrom(__DIR__ . "/{$module}/Views", $module);
            }

        }
    }

    /**
     * Load modules automatically using path config
     */
    protected function boot_auto() {
        $modules_path = __DIR__;
        $finder = Finder::create();
        $modules = $finder->directories()->in($modules_path)->depth('< 1');

        foreach($modules as $module_obj) {

            if( is_object($module_obj) && $module_obj->isDir() )
                $module = $module_obj->getFileName();

            // load routes file in module
            if(file_exists(__DIR__ . "/{$module}/routes.php")) {
                include_once __DIR__ . "/{$module}/routes.php";
            }

            // load files in Views folder
            if(is_dir(__DIR__ . "/{$module}/Views")) {
                $this->loadViewsFrom(__DIR__ . "/{$module}/Views", $module);
            }
        }
    }

    public  function register() {
        // pass
    }

}