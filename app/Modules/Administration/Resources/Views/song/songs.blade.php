@extends('administration::layout.main')

@section('content')
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Songs</h3>
                </div>
                <div class="pull-right">
                    <a href="{{ route('admin.song.create') }}" class="btn btn-primary">Add Song</a>
                </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12">
                    <div class="x_panel">
                        <div class="x_content">

                            @if($songs->isEmpty())
                                <p>No song available.</p>
                                @else
                                        <!-- start project list -->
                                <table class="table table-striped projects">
                                    <thead>
                                    <tr>
                                        <th style="width: 1%">#</th>
                                        <th style="width: 20%">Name</th>
                                        <th style="width: 20%">Artist</th>
                                        <th style="width: 20%">Genre</th>
                                        <th style="width: 20%">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($songs as $key => $song)
                                        <tr>
                                            <td>{{  ++$key }}</td>

                                            <td>
                                                <a>{{ $song->title }}</a>
                                                <br>
                                                <small>Created {{ $song->created_at->toFormattedDateString() }}</small>
                                            </td>

                                            <td> {{ $song->artist->title }} </td>
                                            <td>
                                                <ol>
                                                    @foreach( $song->genres as $genre )
                                                        <li> {{ $genre->genre }} </li>
                                                    @endforeach
                                                </ol>
                                            </td>

                                            <td>
                                                <a href="{{ route('admin.song.show', $song->id) }}" class="btn btn-info btn-xs"><i class="fa fa-folder"></i> View </a>
                                                <a href="{{ route('admin.song.edit', $song->id) }}" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Edit </a>
                                                <form method="post" action="{{ route('admin.song.destroy', $song->id) }}" style="display: inline">
                                                    {{ method_field('DELETE') }}
                                                    {{ csrf_field() }}
                                                    <button type="submit" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Delete </button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                <!-- end project list -->
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{ $songs->links() }}
@endsection