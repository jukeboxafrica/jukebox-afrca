<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Online Music Distribution Platform" />
    <meta name="author" content="Kingsley-Budali" />
    <script type="text/javascript" src="{{asset('js/modernizr.custom.86080.js')}}"></script>
    <link rel="stylesheet" type="text/css" href="{{asset('vendors/css/normalize.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('vendors/css/grid.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}">
    <link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,300italic' rel='stylesheet' type='text/css'>


    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Jukebox Africa') }}</title>

    <!-- Styles -->
   

    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>
<body>
    <header>
<!-- MAIN NAVIGATION --> 
            <nav>
                <div class="row">
                    <img  src="{{asset('img/logo-white1.png')}}" alt="jukeboxgh logo" class="logo">
                    <ul class="main-nav">
                        <li><a href="#">Discover</a></li>
                        <li><a href="#">Genre</a></li>
                        <li><a href="{{route('account')}}">Account</a></li>
                        <!--<li><a href="Signup.html">Sign up</a></li>-->                  
                    </ul>
                </div>
            </nav>
<!-- END MAIN NAVIGATION --> 
            
<!-- SLIDESHOW SPANS -->            
        <ul class="cb-slideshow">
            <li><span>Image 01</span></li>
            <li><span>Image 02</span></li>
            <li><span>Image 03</span></li>
            <li><span>Image 04</span></li>
            <li><span>Image 05</span></li>
            <li><span>Image 06</span></li>
        </ul>
<!---END SPAN--> 
            
            
<!--MAIN HERO IMAGE ND TEXT-BOX WITH BUTTONS--> 
            <div class="hero-text-box">
                <h1>Be the first to get the latest hits<br>from your favourite artists.</br></h1>
                <a class="btn btn-full"href="Signup.html">Sign Up</a>
                <a class="btn btn-ghost"href="#">learn more</a>
            </div>
 <!--HERO END-->
        </header> 
    @yield('content')    
    <!-- Scripts -->
<script src="/js/app.js"></script>
    @yield('footer')
</body>
</html>