<?php

namespace App\Modules\Administration\Http\Controllers;

use App\Album;
use App\Artist;
use App\Genre;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Song;
use Illuminate\Support\Facades\Storage;

class SongsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $songs= Song::paginate(15);
//        foreach ($songs as $song){
//
//            foreach ($song->genre() as $genre){
//                echo $genre->genre;
//            }
//        }
//        dd($songs);
        return view('administration::song.songs', compact('songs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $artists = Artist::all();
        $genres = Genre::all();
        $albums = Album::all();
        return view('administration::song.add',compact('artists','genres','albums'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        dd($request->all());
        //
//        $request->album_id = decrypt($request->album_id);
//        $request->artist_id = decrypt($request->artist_id);
//        $request->genre_id = decrypt($request->genre_id);
        $ref_id = uniqid();
        $file_path = $request->file("music_file")->store('tracks');
        $song = Song::create([
            'ref_id' => $ref_id ,
            'title' =>  $request->title,
            'album_id' =>  $request->album_id,
//            'genre_id' =>  $request->genre_id,
            'artist_id' =>  $request->artist_id,
            'file_path' =>  $file_path,
        ]);
        
        $song->genres()->attach($request->genre_id);

         self::success('Alright! Song added successfully');
         return redirect(route('song.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param $ref_id
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function show($ref_id)
    {
        //
        $song =Song::findOrFail($ref_id);
//        dd($song);
        return view('administration::song.view', compact('song'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $ref_id reference id
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function edit($ref_id)
    {
        //
        $song =Song::findOrFail($ref_id);
        dd($song);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
