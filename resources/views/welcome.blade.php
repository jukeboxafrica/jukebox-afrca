@extends('layouts.frontend')

@section('content')
<section class="section-meals">
            <ul class="meals-showcase clearfix">
                <div class="gamesfont">                    
                    <p>FREE MUSIC <a class="btn btn-full" href="Signup.html">more</a> </p>               
                </div>
                <li>
                    <figure class="meal-photo">                        
                        <a href="https://play.google.com/store/apps/details?id=com.budalistudios.orm" target="_blank"><img src="{{asset('img/1.jpg')}}"  alt="Korean bibimbap with egg and vegetables"></a>
                        <h2>
                            <a class="title" href="" tittle="Just Chris" aria-hidden="true" tabindex="-1">Collide<span class="paragraph-end"></a>
                         </h2>
                        <div class="subtitle-container">
                            <a class="subtitle" href="#" title="Just Chris">Just Chris</a>
                            <span class="price-container">
                                <span class="paragraph-end"></span>
                                <span class="display-price">free</span>   
                            </span>
                        </div>        
                    </figure>
                </li>
                <li>
                    <figure class="meal-photo">                    
                        <img src="{{asset('img/2.jpg')}}" alt="Simple italian pizza with cherry tomatoes">                   
                        <h2>
                            <a class="title" href="" tittle="Just Chris" aria-hidden="true" tabindex="-1">Collide<span class="paragraph-end"></a>
                         </h2>
                        <div class="subtitle-container">
                            <a class="subtitle" href="#" title="Just Chris">Just Chris</a>
                            <span class="price-container">
                                <span class="paragraph-end"></span>
                                <span class="display-price">free</span>   
                            </span>
                        </div>                              
                    </figure>
                </li>
                <li>
                    <figure class="meal-photo">
                        <img src="{{asset('img/3.jpg')}}" alt="ae ruga gone">                      
                            <h2>
                            <a class="title" href="" tittle="Ace Ruga" aria-hidden="true" tabindex="-1">Gone<span class="paragraph-end"></a>
                         </h2>
                        <div class="subtitle-container">
                            <a class="subtitle" href="#" title="Ace Ruga">Ace Ruga</a>
                            <span class="price-container">
                                <span class="paragraph-end"></span>
                                <span class="display-price">free</span>   
                            </span>
                                </div>   
                    </figure>
                </li>
                <li>
                    <figure class="meal-photo">
                        <img src="{{asset('img/wyllz-radio-ft-jae-ghost.jpg')}}" alt="wyllz radio">
                           <!-----------------------music info-------------------------->                      
                            <h2>
                            <a class="title" href="" tittle="wyllz" aria-hidden="true" tabindex="-1">Radio<span class="paragraph-end"></a>
                         </h2>
                        <div class="subtitle-container">
                            <a class="subtitle" href="#" title="Wyllz">Wyllz-JaeGhost</a>
                            <span class="price-container">
                                <span class="paragraph-end"></span>
                                <span class="display-price">free</span>   
                            </span>
                                </div>
   <!-------------------------music info closed---------------------------------->     
                    </figure>
                </li>
            </ul>
            <ul class="meals-showcase clearfix">
                <li>
                    <figure class="meal-photo">
                        <img src="{{asset('img/justchris-colideandconnect-ft-pheanixrose.jpg')}}" alt="Paleo beef steak with vegetables">
                           <!-----------------------music info------------------------------------>                      
                            <h2>
                            <a class="title" href="" tittle="Just Chris" aria-hidden="true" tabindex="-1">Collide<span class="paragraph-end"></a>
                         </h2>
                        <div class="subtitle-container">
                            <a class="subtitle" href="#" title="Just Chris">Just Chris</a>
                            <span class="price-container">
                                <span class="paragraph-end"></span>
                                <span class="display-price">free</span>   
                            </span>
                                </div>
   <!-------------------------music info closed---------------------------------->     
                    </figure>
                </li>
                <li>
                    <figure class="meal-photo">
                        <img src="{{asset('img/aceruga-ride-or-die.jpg')}}" alt="Healthy baguette with egg and vegetables">
                           <!-----------------------music info------------------------------------>                      
                            <h2>
                            <a class="title" href="" tittle="Just Chris" aria-hidden="true" tabindex="-1">Collide<span class="paragraph-end"></a>
                         </h2>
                        <div class="subtitle-container">
                            <a class="subtitle" href="#" title="Just Chris">Just Chris</a>
                            <span class="price-container">
                                <span class="paragraph-end"></span>
                                <span class="display-price">free</span>   
                            </span>
                                </div>
   <!-------------------------music info closed---------------------------------->     
                    </figure>
                </li>
            </ul>
        </section>
<!--------------------END SECOND SECTION--------------------->       
        
<!-----------------------------------------------------------> 
<!----------------------SECOND SECTION-----------------------> 
<!--------------------------------------------------------> 
  <section class="section-features">
        <div class="row">
            
        </div>
         <ul class="meals-showcase clearfix">
                <div class="gamesfont">
                    
                    <p>
                     NEW MUSIC
                        <a class="btn btn-full"href="Signup.html">more</a>
                    </p>
                    
                
                </div>
                <li>
                    <figure class="meal-photo">
                        
                        <a href="#" target="_blank"><img src="{{asset('img/2.jpg')}}"  alt="just chris"></a>
                    </figure>
                </li>
                <li>
                    <figure class="meal-photo">
                        <img src="{{asset('img/2.jpg')}}" alt="just chris">
                    </figure>
                </li>
                <li>
                    <figure class="meal-photo">
                        <img src="{{asset('img/2.jpg')}}" alt="just chris">
                    </figure>
                </li>
                <li>
                    <figure class="meal-photo">
                        <img src="{{asset('img/2.jpg')}}" alt="just chris">
                    </figure>
                </li>
            </ul>
            <ul class="meals-showcase clearfix">
                <li>
                    <figure class="meal-photo">
                        <img src="{{asset('img/2.jpg')}}" alt="just chris">
                    </figure>
                </li>
                <li>
                    <figure class="meal-photo">
                        <img src="{{asset('img/2.jpg')}}" alt="just chris">
                    </figure>
                </li>
            </ul>
      
    </section>
<!--------------------END SECOND SECTION--------------------->   
    <!-----------------------------------------------------------> 
<!----------------------SECOND SECTION-----------------------> 
<!--------------------------------------------------------> 
  <section class="section-popularsingles">
        <div class="row">
            
        </div>
         <ul class="meals-showcase clearfix">
                <div class="gamesfont">
                    
                    <p>
                     POPULAR SINGLES
                        <a class="btn btn-full"href="Signup.html">more</a>
                    </p>
                    
                
                </div>
                <li>
                    <figure class="meal-photo">
                        
                        <a href="#" target="_blank"><img src="{{asset('img/Right-Now-Track-Cover.jpg')}}"  alt="Wyllz"></a>
                    </figure>
                </li>
                <li>
                    <figure class="meal-photo">
                        <img src="{{asset('img/BEV.jpg')}}" alt="Wyllz">
                    </figure>
                </li>
                <li>
                    <figure class="meal-photo">
                        <img src="{{asset('img/3.jpg')}}" alt="just chris">
                    </figure>
                </li>
                <li>
                    <figure class="meal-photo">
                        <img src="{{asset('img/3.jpg')}}" alt="just chris">
                    </figure>
                </li>
            </ul>
            <ul class="meals-showcase clearfix">
                <li>
                    <figure class="meal-photo">
                        <img src="{{asset('img/3.jpg')}}" alt="just chris">
                    </figure>
                </li>
                <li>
                    <figure class="meal-photo">
                        <img src="{{asset('img/3.jpg')}}" alt="just chris">
                    </figure>
                </li>
            </ul>
      
    </section>

    @endsection