<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Song extends Model
{
    //
    use SoftDeletes;
    protected $primaryKey = 'ref_id';
    public $incrementing = false;

    protected $fillable =['ref_id','title','album_id','artist_id','price','genre_id','file_path'];
    protected $hidden =['file_path'];
    protected $dates =['deleted_at' , 'created_at' , 'updated_at'];
    protected  $softDeletes = true;

    public function genre(){
        return $this->belongsTo('App\Genre');
    }

    public function artist(){
        return $this->belongsTo('App\Artist');
    }

    /**
     * Relation function to get the payment details of an order
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function genres(){
        return $this->belongsToMany('App\Genre');
    }
}
