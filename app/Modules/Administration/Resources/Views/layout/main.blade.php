
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Dashboard</title>

    <!-- Bootstrap -->
    <link href="{{ asset('assets/modules/admin/vendor/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{ asset('assets/modules/admin/vendor/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
    <!-- NProgress -->
    <link href="{{ asset('assets/modules/admin/vendor/nprogress/nprogress.css') }}" rel="stylesheet">
    <!-- PNotify -->
    <link href="{{ asset('assets/modules/admin/vendor/pnotify/dist/pnotify.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/modules/admin/vendor/pnotify/dist/pnotify.buttons.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/modules/admin/vendor/pnotify/dist/pnotify.nonblock.css') }}" rel="stylesheet">

    <!-- Select2 -->
    <link href="{{ asset('assets/modules/admin/vendor/select2/dist/css/select2.css') }}" rel="stylesheet">

    <!-- Custom styling plus plugins -->
    <link href="{{ asset('assets/modules/admin/css/custom.min.css') }}" rel="stylesheet">

    <!-- Additional -->
    @yield('style')
</head>

<body class="nav-md">
<div class="container body">
    <div class="main_container">

        @include('administration::includes.left-navigation')
                <!-- top navigation -->
        @include('administration::includes.top-navigation')
                <!-- /top navigation -->

        <!-- page content -->
        @yield('content')
                <!-- /page content -->

    </div>
</div>

<!-- jQuery -->
<script src="{{ asset('assets/modules/admin/vendor/jquery/dist/jquery.min.js') }}"></script>
<!-- Bootstrap -->
<script src="{{ asset('assets/modules/admin/vendor/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<!-- FastClick -->
<script src="{{ asset('assets/modules/admin/vendor/fastclick/lib/fastclick.js') }}"></script>
<!-- NProgress -->
<script src="{{ asset('assets/modules/admin/vendor/nprogress/nprogress.js') }}"></script>
<!-- Select2 -->
<script src="{{ asset('assets/modules/admin/vendor/select2/dist/js/select2.js') }}"></script>

<!-- Custom Theme Scripts -->
<script src="{{ asset('assets/modules/admin/js/custom.min.js') }}"></script>

<!-- PNotify -->
<script src="{{ asset('assets/modules/admin/vendor/pnotify/dist/pnotify.js') }}"></script>
<script src="{{ asset('assets/modules/admin/vendor/pnotify/dist/pnotify.buttons.js') }}"></script>
<script src="{{ asset('assets/modules/admin/vendor/pnotify/dist/pnotify.nonblock.js') }}"></script>

<!-- Additional -->
@yield('js')

@if(session('flash'))
    @foreach(session('flash') as $key => $message)
        <script>
            new PNotify({
                title: 'Notification',
                text: '{{ $message }}',
                type: '{{ $key }}',
                styling: 'bootstrap3'
            });
        </script>
    @endforeach
@endif
</body>
</html>