<?php

namespace App\Modules\Administration\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
class DashboardController extends Controller
{
    //
    public function index(){
        return view('administration::dashboard.index');
    }
}
