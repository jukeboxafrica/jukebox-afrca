<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGenreSongsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('genre_song', function (Blueprint $table) {
            $table->string('genre_id')->nullable();
            $table->string('song_id');

            $table->unique(['song_id','genre_id']);

            $table->foreign('genre_id')->references('ref_id')->on('genres')->onDelete('set null')->onUpdate('cascade');
            $table->foreign('song_id')->references('ref_id')->on('songs')->onDelete('cascade')->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('genre_song');
    }
}
