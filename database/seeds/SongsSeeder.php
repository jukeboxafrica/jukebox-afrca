<?php

use Illuminate\Database\Seeder;

class SongsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $songs=[
            [
                'ref_id'=> 1 ,
                'title'=>'Sorry',
//                'genre_id'=>'1',
                'artist_id'=>'1',
                'price'=> '50',
                'file_path'=>'songs',
            ],
            [
                'ref_id'=> 2 ,
                'title'=>'HAte you bad',
//                'genre_id'=>'1',
                'artist_id'=>'1',
                'price'=> '50',
                'file_path'=>'songs',
            ],
            [
                'ref_id'=> 3 ,
                'title'=>'Nomore',
//                'genre_id'=>'1',
                'artist_id'=>'1',
                'price'=> '50',
                'file_path'=>'songs',
            ]
        ];

        foreach ($songs as $key => $value) {
            \App\Song::create($value);
        }
    }
}
