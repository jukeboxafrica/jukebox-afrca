<?php
/**
 * Created by PhpStorm.
 * User: edel1
 * Date: 10/6/2016
 * Time: 8:34 AM
 */

namespace App\Customs;


trait FlashMessage
{
    protected static function message($level = 'info', $message = null) {
        if(session()->has('messages')) {
            $messages = session()->pull('messages');
        }
        $messages[] = $message = ['level' => $level, 'message' => $message];
        //dd('fm', $messages);
        session()->flash('messages', $messages);
        return $message;
    }

    protected static function messages() {
        return self::hasMessages() ? session()->pull('messages') : [];
    }

    protected static function hasMessages() {
        return session()->has('messages');
    }

    protected static function success($message) {
        return self::message('success', $message);
    }

    protected static function info($message) {
        return self::message('info', $message);
    }

    protected static function warning($message) {
        return self::message('warning', $message);
    }

    protected static function error($message) {
        return self::message('danger', $message);
    }

}