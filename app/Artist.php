<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
class Artist extends Model
{
    //
    use Notifiable,SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstname','lastname','email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password'
    ];

    protected  $dates =['deleted_at','updated_at','created_at'];
    protected  $softDeletes = true;

    /**
     * Gets all the songs of an artist
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function songs(){
        return $this->hasMany('App\Song');
    }



}
