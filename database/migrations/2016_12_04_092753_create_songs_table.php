<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSongsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('songs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ref_id')->unique();
            $table->string('title');
            $table->string('album_id')->nullable();
            $table->string('artist_id');
//            $table->string('genre_id')->nullable(); @ Reason changeto a many to many concept
            $table->string('price')->nullable();
            $table->string('file_path');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('album_id')->references('ref_id')->on('albums')->onDelete('set null')->onUpdate('cascade');
            $table->foreign('artist_id')->references('ref_id')->on('artists')->onDelete('cascade')->onUpdate('cascade');
//            $table->foreign('genre_id')->references('ref_id')->on('genres')->onDelete('set null')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('songs');
    }
}
